'use strict';

const path = require('path');
const fs = require('fs');
const dotEnv = require('dotenv');

function loadDotEnv() {
    // .env is not in cvs so if it does not exist its being installed as child

    let fileName = process.env.NODE_ENV === 'production' ? '.env-prod' : '.env';
    let filePath;
    try {
        filePath = path.resolve('.', fileName);
        fs.statSync(filePath).isFile();
    } catch (e) {
        // check the parent
        try {
            filePath = path.resolve('../../', fileName);
            fs.statSync(path.resolve('./../../', fileName)).isFile();
            // eslint-disable-next-line no-shadow
        } catch (e) {
            // fallback
            if (process.env.NODE_ENV) {
                // eslint-disable-next-line no-console
                console.log('Unable to load .env-prod, searching for .env');
                try {
                    fileName = '.env';
                    filePath = path.resolve('.', fileName);
                    fs.statSync(filePath).isFile();
                    // eslint-disable-next-line no-shadow
                } catch (e) {
                    // check the parent
                    try {
                        filePath = path.resolve('../../', fileName);
                        fs.statSync(
                            path.resolve('./../../', fileName),
                        ).isFile();
                        // eslint-disable-next-line no-shadow
                    } catch (e) {
                        /* noop */
                    }
                }
            }
        }
    }
    console.log('Will attempt to load env from ', filePath); // eslint-disable-line no-console
    dotEnv.config({ path: filePath });
}

module.exports = loadDotEnv;
